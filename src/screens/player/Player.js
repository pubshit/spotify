import './player.css'
import {useLocation} from "react-router-dom";
import {useEffect, useState} from "react";
import apiClient from "../../Spotify";
import SongCard from "../../components/SongCard/SongCard";
import Queue from "../../components/Queue/Queue";
import AudioPlayer from "../../components/AudioPlayer/AudioPlayer";
import Widgets from "../../components/widget/Widgets";

export default function Player() {
    const location = useLocation()

    const [tracks, setTracks] = useState([]);
    const [currentTrack, setCurrentTrack] = useState({});
    const [currentIndex, setCurrentIndex] = useState(0);

    useEffect(() => {
        if (location.state) {
            apiClient.get(`playlists/${location.state?.id}/tracks`).then(res => {
                setTracks(res.data.items)
            })
        }
    }, [location.state]);

    useEffect(() => {
        setCurrentTrack(tracks[currentIndex]?.track)
    }, [currentIndex, tracks]);

    return (
        <div className="screens-container flex">
            <div className="left-player-body">
                <AudioPlayer
                    currentTrack={currentTrack}
                    total={tracks}
                    currentIndex={currentIndex}
                    setCurrentIndex={setCurrentIndex}
                />
                <Widgets artistID={currentTrack?.album} />
            </div>
            <div className="right-player-body">
                <SongCard album={currentTrack?.album}/>
                <Queue tracks={tracks} setCurrentIndex={setCurrentIndex}/>
            </div>
        </div>
    )
}