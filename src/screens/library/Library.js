import APIkit from '../../Spotify'
import {useEffect, useState} from "react"
import './library.css'
import {useNavigate} from "react-router-dom"
import {BsFillPlayFill} from "react-icons/bs"
import * as React from "react"



export default function Library() {



    const [playlists, setPlaylists] = useState([]);

    useEffect(() => {

        APIkit.get('me/playlists').then(res => {
            setPlaylists(res.data.items)
        })

    }, []);


    const navigate = useNavigate()

    const playPlaylist = (id) => {
        navigate('/player', {state: {id: id}})
    }





    return (
        <div className="screens-container">
            <div className="library-container">
                <div className="playlist-liked">
                    <h1 className="title-playlist-liked">Liked Songs</h1>
                    <div className="icon-container">
                        <BsFillPlayFill className="icon-likedPlaylist"/>
                    </div>
                </div>
                {playlists?.map(playlist => (
                    <div className="playlists-item" key={playlist.id} onClick={() => playPlaylist(playlist.id)}>
                        <div className="image-container">
                            <img src={playlist.images[0].url} alt="Playlist Art" className="playlist-image"/>
                        </div>
                        <div className="effect-1"></div>
                        <div className="effect-2"></div>
                        <p className="playlist-name">{playlist.name}</p>
                        <p className="playlist-description">{playlist.description}</p>
                        <p className="playlist-tracksTotal">{playlist.tracks.total} Songs</p>
                        <div className="playlist-fade">
                            <div className="icon-container">
                                <BsFillPlayFill size="40px" color="white"/>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    )
}