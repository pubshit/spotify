import {loginEndpoint} from "../../Spotify";
import './login.css'


export default function Login() {

    return(
        <div className="login-page">
            <img src="https://www.pngkey.com/png/full/190-1907978_spotify-logo-png-white-spotify-logo-white-transparent.png"
                 alt="logo-spotify"
                 className="logo"/>
            <a href={loginEndpoint} >
                <div className="login-btn">
                    LOGIN
                </div>
            </a>
        </div>
    )
}