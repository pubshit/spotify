import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import React from "react";
import {IconContext} from "react-icons";
import {Link, useLocation} from "react-router-dom";
import './SideBarButton.css'


export default function SideBarButton(props) {
const location = useLocation()

    const isActive = location.pathname === props.to
    const ClassBtn = isActive ? 'button-container active' : 'button-container'

    return (
        <>
            <Link to={props.to} className={ClassBtn}>
                <ListItemAvatar>
                    <IconContext.Provider value={{size: '24px'}}>
                        {props.icon}
                        <ListItemText>
                            {props.title}
                        </ListItemText>
                    </IconContext.Provider>
                </ListItemAvatar>
            </Link>
        </>
    )
}