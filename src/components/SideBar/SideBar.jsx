import * as React from 'react';
import {Container, Paper, Grid} from "@mui/material";
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import './sideBar.css'
import SideBarButton from "./Button/SideBarButton";
import {MdDashboard, MdFavorite} from 'react-icons/md'
import {BsFillHeartFill, BsFillPlayFill} from 'react-icons/bs'
import {RiFireFill} from 'react-icons/ri'
import {GoSignOut} from 'react-icons/go'
import {ImBooks} from 'react-icons/im'
import {useState, useEffect} from "react";
import apiClient from "../../Spotify";



export default function SideBar() {
    const [image, setImage] = useState('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRdLAY3C19kL0nV2bI_plU3_YFCtra0dpsYkg&usqp=CAU');

    useEffect(() => {
        apiClient.get("me").then((response) => {
        });
    }, []);

    return (
        <Box className='sidebar-container'>
            <Grid className="sidebar-header">
                <img
                    src={image}
                    className="profile-img"
                    alt=""
                />
            </Grid>
            {/* Content */}
            <div className="sidebar-content">
                {/* Feed */}
                <SideBarButton title="Feed" to="/feed" icon={<MdDashboard/>}/>
                {/* Trending */}
                <SideBarButton title="Trending" to="/trending" icon={<RiFireFill/>}/>
                {/* Player */}
                <SideBarButton title="Player" to="/player" icon={<BsFillPlayFill/>}/>
                {/* Favorites */}
                <SideBarButton title="Favorites" to="/favorites" icon={<MdFavorite/>}/>
                {/* Library */}
                <SideBarButton title="Library" to="/" icon={<ImBooks/>}/>
            </div>

            {/* Footer */}
            <div className="sidebar-footer">
                <Divider variant="middle" component="div" sx={{border:'0.5px solid #ccc'}}/>
                <SideBarButton title="" to="" icon={<GoSignOut/>}/>
            </div>
        </Box>
    )
}