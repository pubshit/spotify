import './songcard.css'
import AlbumImage from "./AlbumImage/albumImage";
import AlbumInfo from "./AlbumInfo/albumInfo";
import * as React from "react";

export default function SongCard({album}) {


    return (
        <div className="songCard-body flex ">
            <AlbumImage url={album?.images[0]?.url}/>
            <AlbumInfo album={album}/>
        </div>
    )
}