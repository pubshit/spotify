import './queue.css'

export default function Queue({tracks, setCurrentIndex}) {


    const handleDuration = (track,index) => {
        var trackTime = (track?.track?.duration_ms * 1.7 * 0.00001).toString()
        var time = ''

        for (let i = 0; i < trackTime.toString().length; i++) {
            if (trackTime.toString().charAt(i) !== '.')
                time += trackTime.toString().charAt(i)
            else if (trackTime.toString().charAt(i) === '.') {
                time += (':' + trackTime.toString().charAt(i + 1) + trackTime.toString().charAt(i + 2))
                break
            }
        }




        return (
            <div className="queue-item-container flex"
                 onClick={(e)=>setCurrentIndex(index)}
                 key={track.track.id}
            >
                <div className="queue-item flex" >
                    <p className="track-name">{track?.track?.name}</p>
                    <p>{time}</p>
                </div>
                <div className="divider"></div>
            </div>
        )
    }
    const handleNextTrack = (e,index) => {
        console.log(index)
        e.preventDefault()
        setCurrentIndex(index)
    }

    return (
        <div className="queue-container flex">
            <div className="queue flex">
                <p className="upNext">Next Song</p>
                <div className="queue-list">
                    {tracks?.map((track,index) => handleDuration(track,index))}
                </div>
            </div>
        </div>
    )
}