import './WidgetEntry.css'
import React from 'react'


export default function WidgetEntry({image, title, subtitle}) {


    return (
        <div className="entry-body flex">
            <img src={image} alt={title} className="entry-image"/>
            <div className="entry-right-body flex">
                <p className="entry-title">{title}</p>
                <p className="entry-subtitle">{subtitle}</p>
            </div>
        </div>
    )
}
