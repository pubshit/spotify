import './widgetCard.css'
import WidgetEntry from "../widgetEntry/WidgetEntry";
import {CgChevronRight} from "react-icons/cg";

export default function WidgetCard({similar, title, featured, newRelease}) {
    // console.log(similar)
    // console.log(featured)
    // console.log(newRelease)
    return (
        <div className="widgetCard-body">
            <div className="effect1"></div>
            <div className="effect2"></div>
            <p className="widget-title">
                {title}
            </p>
            {
                similar ?
                    similar?.map(artist => (
                        <div key={artist.id}>
                            <WidgetEntry
                                title={artist?.name}
                                subtitle={artist?.followers?.total + " Followers"}
                                image={artist?.images[0].url}
                            />
                        </div>
                    ))
                    :
                    featured ?
                        featured.map(playlist => (
                            <div key={playlist.id}>
                                <WidgetEntry
                                    title={playlist?.name}
                                    subtitle={playlist?.tracks.total + " Songs"}
                                    image={playlist?.images[0]?.url}
                                />
                            </div>
                        ))
                        :
                        newRelease ?
                            newRelease.map(album => (
                                <div key={album.id}>
                                    <WidgetEntry

                                        title={album?.name}
                                        subtitle={album?.artists[0]?.name}
                                        image={album?.images[2]?.url}
                                    />
                                </div>
                            ))
                            :
                            null
            }
            <div className="widget-fade flex">
                <div className="fade-btn">
                    <CgChevronRight />
                </div>
            </div>
        </div>
    )
}