import './controls.css'
import {IoPlaySkipForward, IoPlaySkipBack, IoPlay, IoPause} from "react-icons/io5";

export default function Controls({isPlaying, setIsPlaying, HandlePrev, HandleNext}) {

    return (
        <div className="controls-wrapper flex">
            <div className="action-btn flex" onClick={HandlePrev}>
                <IoPlaySkipBack/>
            </div>
            <div
                className={isPlaying ? "play-pause-btn flex active" : "play-pause-btn flex"}
                onClick={()=>setIsPlaying(!isPlaying)}
            >
                { isPlaying===true? <IoPause /> : <IoPlay />}
                {/*<IoPlay/>*/}
            </div>
            <div className="action-btn flex" onClick={HandleNext}>
                <IoPlaySkipForward/>
            </div>
        </div>
    )
}